# Sensors

**External ADC** 

[MCP3208](https://ww1.microchip.com/downloads/en/DeviceDoc/21298e.pdf) : Standard ADC unit having 8 multiplexed channel 12 bit ADC with SPI interface. 


**Temperature**

[MCP9700A](https://www.farnell.com/datasheets/2243473.pdf) : Cheap junction based temperature sensor (-40°C to +125°C, +-2%)

**Windvane**



**Anemometer**

[NRG40](https://www.nrgsystems.com/assets/resources/NRG-40-Tech-Product-Sheet-2020-V1.pdf) : This is a high-end sinewave based anemometer supported by re-innovation module.

[Analog metal anemometer](https://fr.aliexpress.com/item/32798148991.html?gatewayAdapt=glo2fra) : This is a cheap anemometer supported by re-innovation module.

**Current**

[current sensor](https://fr.aliexpress.com/item/32836805934.html?src=google&src=google&memo1=freelisting&albch=shopping&acnt=248-630-5778&isdl=y&slnk=&plac=&mtctp=&albbt=Google_7_shopping&aff_platform=google&aff_short_key=UneMJZVf&albagn=888888&isSmbAutoCall=false&needSmbHouyi=false&albcp=10191220526&albag=107473525328&trgt=1284054470089&crea=fr32836805934&netw=u&device=c&albpg=1284054470089&albpd=fr32836805934&gclid=CjwKCAiA-9uNBhBTEiwAN3IlNFCuFdAqd_xfRvwiwjnRE5MmpalQLLTFTOurUwq17v1V1YVrhZXepxoC9kwQAvD_BwE&gclsrc=aw.ds)

**Voltage**





# Integrated circuits

[FS8205A](https://ic-fortune.com/upload/Download/FS8205A-DS-12_EN.pdf) : Dual N-Channel Enhancement Mode Power MOSFET for Lion battery management

[FS312F-G](http://datasheet.elcodis.com/pdf2/98/28/982820/fs312f-g.pdf) : One Cell Lithium-ion/Polymer Battery Protection IC

[TP4056](http://dlnmh9ip6v2uc.cloudfront.net/datasheets/Prototyping/TP4056.pdf) : Standalone Linear Li-lon Battery Charger with Thermal
Regulation

[ESP32WROOM32U](https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32d_esp32-wroom-32u_datasheet_en.pdf) : IoT microcontroller. Not recommended for new designs, why ?

[OPA330](https://www.ti.com/lit/ds/symlink/opa330.pdf?ts=1643234890298&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FOPA330) : opAmp for current and voltage interfaces

[B0303S-WR2 :](https://pdf1.alldatasheet.com/datasheet-pdf/view/1102436/MORNSUN/B0303S-1WR2.html) isolated DC/DC converter (3V3 to 3V3)

[AMC1200DUBR](https://www.ti.com/lit/ds/symlink/amc1200.pdf?ts=1643235325938&ref_url=https%253A%252F%252Fwww.ti.com%252Fstore%252Fti%252Fen%252Fp%252Fproduct%252F%253Fp%253DAMC1200SDUBR) : Fully-Differential Isolation Amplifier (voltage measurement)

[MCP1700](https://ww1.microchip.com/downloads/en/DeviceDoc/MCP1700-Low-Quiescent-Current-LDO-20001826E.pdf) : LDO for battery management

[LM2596S](https://www.ti.com/general/docs/suppproductinfo.tsp?distId=26&gotoUrl=http%3A%2F%2Fwww.ti.com%2Flit%2Fgpn%2Flm2596) : Power supply

# External modules

[**Anemometer and windvane interface**](https://www.re-innovation.co.uk/docs/anemometer-monitor-for-wind-measurements/) --> github repo [here](https://github.com/curiouselectric/WindSensor)

[SD card manager](https://fr.aliexpress.com/item/32808562025.html?spm=a2g0o.productlist.0.0.590769b3K5jE16&algo_pvid=3803bd50-7c0e-44ea-afc8-7e57e2f9c709&algo_exp_id=3803bd50-7c0e-44ea-afc8-7e57e2f9c709-14&pdp_ext_f=%7B%22sku_id%22%3A%2264478442589%22%7D&pdp_pi=-1%3B0.28%3B-1%3B-1%40salePrice%3BEUR%3Bsearch-mainSearch) 
