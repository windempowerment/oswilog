# OSWiLog

Open Small Wind Logger (OSWiLog) is the WindEmpowerment Open Source Hardware datalogger project for Small Wind Turbines (SWT).

## Getting started

* Prerequisite : 
   * KiCAD
   * Git
* Get started by cloning the repo. ```git clone https://gitlab.com/windempowerment/oswilog.git```
* Launch ```KiCAD_files/datalogger.pro``` with KiCAD. 
* Enjoy. 



## Needs

As evidenced by ([Leary et al., 2020](https://journals.sagepub.com/doi/full/10.1177/0309524X20932501)), locally manufactured small wind turbines (LMSWT) could be over or underperforming for many reasons. This evidences the need of a tool to detect a misfunctioning LMSWT installation. A practicioner or a particular should be able to follow a small wind turbine energy production and prevent a misfunctioning installation to underperform too long by playing on some parameters of the installation (e.g. furling system). In a long term perspective, it is interesting for the LMSWT community to share the measured data to evidence some good and bad practices of systems troubleshooting and enable scientific research analysis.

This initiative comes from members of Wind Empowerment, an association for the development of small wind turbines based on Hugh Piggott's designs and for sustainable rural electrification. A major goal of the association is to help its members (and the small wind community) to improve the quality of the fabrication and installation of small wind turbines. Firstly by permitting a site study before an installation and secondly by allowing the improvement of the technology through studying the production. Creating a international test site of machines with a WindData Logger installed. Data logging allows everyone: manufacturers, installers, users, as well as students to better understand and improve the production of a small wind turbine.

Existing data loggers on the market are too expensive, complicated and not adapted to the stated needs.

## Goal

The goal of OSWiLog datalogger project is to dispose of an open source tool, which is simple, robust, cheap and matches the needs of the small wind community. The logger should work right away, should be easy to install, robust. Here are the different data which are needed.

**Power curves : **OSWiLog should be able to collect enough data to plot at least a power curve, which requires wind speed and output dc power. This is the most important goal.

**Generator speed : ** The speed of the generator is a key information for the understanding of the blades-generator matching as well as the furling system behavior regarding rotational speed. This is a secondary goal.

**Dump load current :** The dump load current is useful to measure the proportion of wind energy production which is not used to charge the battery or used for the regular loads. This enables to judge of the oversizing of a system.

To achieve these goals, OSWiLog should be able to log data from multiple sensors : an anemometer and wind vane to measure the wind speed and direction, current and voltage sensors to measure the produced power. 

## Requirements

### Power supply

Powered from a 12 V-48 V external battery. The regulation of the external battery is out of the scope of the project. A poka-yoke is necessary to avoid any reverse battery connection.

### Data display 

2x LED that indicates that the datalogger is working & debugging

(Option) Local display

### Data logging

Local storage on SD card (see proposed [tutorial](https://randomnerdtutorials.com/esp32-data-logging-temperature-to-microsd-card/))

(Option) Remote storage and display.

### I/Os

* (Option) 2/3 IOs to be able to pilot some relays.

### Human to machine interface

2 push buttons

(Option) 1 potentiometer

### PCB

* Dimension 10x10 cm

* SMD components only

* Passive components not below 0805 casing to ease manual repairs

* IC not below 1.27 mm pitch 

* Only widely available component references

* Use components available on LCSC distributor

* Up to 4 layers with vias

* Standard copper thickness (1 oz)

**Note 1**: Using only widely available components is important to ensure that we do not suffer from chip shortage and that people across the world can source components locally to repair the devices.

**Note 2**: Using components available on LCSC distributor makes it possible to source all the components directly from a PCB manufacturer pre-assembled without the need of soldering every small components. This would help keep it low cost and integrated with the chinese manufacturing ecosystem.

### Others

(Option) Waterproof/insectproof case

### Compatibility

The wind measurement should come from this [open source wind sensor interface](https://github.com/curiouselectric/WindSensor) through serial link.



![img](https://www.re-innovation.co.uk/uploads/wind-sensor-overview-1024x312.png)

![img](https://www.re-innovation.co.uk/uploads/Wiring_overview-1024x592.png)



### Sensors

| **Sensor**    |       **Priority**                           | **Range**                | **Desired Precision**      | **Acquisition freq** | **Recording freq** | **Reference** |
| ---------------|-------------------------------- | ------------------------ | -------------------------- | -------------------- | ------------------ | ------------- |
| Wind speed     |                                 | 0-30 m/s                 | <0.5m/s                    | 1 Hz                 | 1 min ave          |               |
| (Option) Wind vane   |        Optional                   | 0-360 deg                | 0.5 deg                    | 1 Hz                 | 1 min ave          |               |
| (Option) RPM      |     Optional                         | 0-1000 rpm               | 1 RPM?                     | 1 Hz                 | 1 min ave          | Note 1        |
| DC voltage (isolated)   |                        | 0-80 			V       |                            | 1 Hz                 | 1 min ave          | Note 6, 9     |
| DC current          |                            | 0-50 			A       | 2%                         | 1 Hz                 | 1 min ave          | Note 7, 8, 12 |
| (Option) 2nd DC voltage  (isolated)|Optional| 0-80 V                   |                            | 1 Hz                 | 1 min ave          | Note 6, 9     |
| (Option) 2nd DC current    |   Optional      | 0-50 A                   | 2%                         | 1 Hz                 | 1 min ave          | Note 7, 8, 12 |
| (Option) Ambiant temperature      |  Optional            | -30-65°C                 | 2°C                        | 1/60 Hz              | 1/60 Hz            | Note 2, 10    |
| (Option) Air pressure        |         Optional          | 780 – 1100 hPA           | Define according to sensor | 1/60 Hz              | 1/60 Hz            | Note 10       |
| (Option) Generic sensor      |         Optional         | 0-10 V, 4-20 mA, impulse | -                          | 1 Hz                 | 1 min ave          |               |


Note 1: Logging the RPM allows one to characterize the blades alone.

Note 2: Logging the Temperature allows one to correct for the variation of the air density.

Note 3: For difference between “acquisition frequency” vs. “recording frequency”, and for recommended values see [Queval2014]. It could be decreased by software to reduce consumption.

Note 4: The optional sensors could be routed on the PCB but only installed when necessary.

Note 5: With a 48 V DC bus, and 50 A max current, the datalogger would be able to measure turbines up to 2.4 kW.

Note 6: Add a note to justify 80 V (Jean) to be checked.

Note 7: Measurement equipment accuracy for measuring current and power see IEC61400-12, Annex H.5 Test equipment.

Note 8: Current transformers shall meet requirements of IEC 61869-2.

Note 9: Voltage transformers shall meet requirements of IEC61869-3.

Note 10: Data sampling rate see IEC61400-12, 8.3 Data collection.

Note 11: Pressure range for installation in altitudes from 0 to aprox. 2.000 m. 

Note 12: The current sensor should be connected via cable to PCB, not directly mounted onto the PCB.


### Software requirements

<u>Minimal</u> : 

* Data are stored on a SD card.

* Standard data string.

  <u>Optional</u> : 

* The data is displayed in real time (small screen already implemented in v0).
* Sent data to an online database via ethernet/wifi.

### Software design
KiCAD + export as gerber file 

### Environmental requirements

(Optional)

| **Quantity** | **Value**     | **Explanation**                                              |
| ------------ | ------------- | ------------------------------------------------------------ |
| Temperature  | - 30 to 65 °C | Ambiant 		temperature                                  |
| Humidity     | 100 %         | Ambiant 		humidity                                     |
| Insects      | Small ;-p     | Presence of 		insects                                  |
| UV           |               | Just to keep 		it in mind when selecting the case      |
| Dust         |               | Just to keep 		it in mind when selecting the connectors and sensors |


## Enclosure 

Suitable enclosure for the PCB was found on Aliexpress including proper electrical screwed terminals.

VG-P46+ https://shorturl.at/cjSU1

![img](Images/VG_P46_Enclosure_drawing.webp)

![img](Images/VG_P46_screw_terminals.webp)

## License

This work is licensed under CERN-OHL-V2-S. Read carefully the license file attached.

## Project status

This project is in an early stage, please note that nothing is proven to work yet. 

## Credits 

Special thanks to Hugo Pietri and his team for the initial proof of concept. 
